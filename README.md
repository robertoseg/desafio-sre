
# Instalação Nginx no centos 7 com Wordpress , Magento , e um site simples

 Obs: Minha recomendação é começar pela instalação do Magento, pois a configuração do PHP  já inicia com  ele para não dar conflito

 Máquina utilizada  t2.micro 
 Recursos utilizados na AWS, Route 53, EC2

**<h2>Instalação</h2>**
**Primeiro, instale o servidor da web Nginx, servidor de banco de dados MariaDB, PHP e outras bibliotecas PHP necessárias executando o seguinte comando:**

```
dnf install nginx mariadb-server php php-cli php-mysqlnd php-opcache php-xml php-gd php-soap php-bcmath php-intl php-mbstring php-json php-iconv php-fpm php-zip unzip git -y

```


**Assim que todos os pacotes estiverem instalados, inicie o serviço Nginx, MariaDB e PHP-FPM e habilite-os para iniciar após a reinicialização do sistema com o seguinte comando:**

```
1.  systemctl start nginx
2.  systemctl start mariadb
3.  systemctl start php-fpm
4.  systemctl enable nginx
5.  systemctl enable mariadb
6.  systemctl enable php-fpm

```
**Em seguida, você precisará editar o arquivo php.ini e ajustar algumas configurações:**

```
vim /etc/php.ini

```
**Altere as seguintes linhas:**
```
memory_limit = 512M
upload_max_filesize = 200M
zlib.output_compression = On 
max_execution_time = 300 
date.timezone = Asia / Kolkata

```
<h2> Configurar banco de dados </h2>

**Por padrão, MariaDB não é protegido, portanto, você precisará protegê-lo primeiro. Execute o seguinte script para proteger o MariaDB:**

```
mysql_secure_installation

```
Perguntas do Mysql 

```
Digite a senha atual para root (digite nenhum): 
Definir senha de root? [S / n] S
Nova senha: 
Re-introduza a nova palavra-passe: 
Remover usuários anônimos? [S / n] S
Desautorizar login de root remotamente? [S / n] S
Remover banco de dados de teste e acesso a ele? [S / n] S
Recarregar tabelas de privilégios agora? [S / n] S

```

**Quando terminar, faça login no shell do MariaDB com o seguinte comando:**

```
mysql -u root -p

```

**Forneça sua senha root quando solicitado, em seguida, crie um banco de dados e usuário para Magento a mostrado abaixo:**
```
CREATE DATABASE magentodb;
GRANT ALL ON magentodb.* TO magento@localhost IDENTIFIED BY 'password';
```
**Em seguida, libere os privilégios e saia do shell MariaDB com o seguinte comando:**
```
flush privileges;
exit;
```
<h1> Configurar PHP-FPM para Magento </h1>

**Adicione as seguintes linhas:**

```
[magento]
user = nginx
group = nginx
listen.owner = nginx
listen.group = nginx
listen = /run/php-fpm/magento.sock
pm = ondemand
pm.max_children = 50
pm.process_idle_timeout = 10s
pm.max_requests = 500
chdir = /
```
**Salve e feche o arquivo e reinicie o serviço PHP-FPM para implementar as alterações:**

```
systemctl restart php-fpm
```
<h2>Download Magento</h2>

**Primeiro, baixe a versão mais recente do Magento do repositório Git usando o seguinte comando:**
```
cd /var/www/html
 wget https://github.com/magento/magento2/archive/2.3.zip
```
**Depois de baixado, descompacte o arquivo baixado conforme mostrado abaixo:**
```
unzip 2.3.zip
```
**Em seguida, mova o diretório extraído para magento2 conforme mostrado abaixo:**
```
mv magento2-2.3 magento2
```
**Em seguida, você precisará instalar o Composer para instalar as dependências do PHP para Magento.
Você pode instalar o Composer com o seguinte comando:**
```
curl -sS https://getcomposer.org/installer | sudo php -- --install-dir=/usr/bin --filename=composer
```
**Vamos mudar o diretório para magento2 e instale todas as dependências**
```
cd /var/www/html/magento2
 composer update
 composer install
```
**Configurando o Nginx para MagentoEm seguida, você precisará criar um arquivo de host virtual Nginx para o Magento. Você pode criá-lo com o seguinte comando:**
```
vim /etc/nginx/conf.d/magento.conf
```
**Adicione as seguintes regras:**
```
upstream fastcgi_backend {
  servidor unix: /run/php-fpm/magento.sock;
}
upstream fastcgi_backend {
  servidor unix: /run/php-fpm/magento.sock;
}

servidor {
    escute 80;
    server_name magento.linuxbuz.com;

    definir $ MAGE_ROOT / var / www / html / magento2;
    definir $ MAGE_MODE desenvolvedor;

    access_log /var/log/nginx/magento-access.log;
    error_log /var/log/nginx/magento-error.log;

    inclua /var/www/html/magento2/nginx.conf.sample;
}
```
Obs: não esqueça de mudar o server name para o seu DNS 
Salve e feche o arquivo quando terminar. Em seguida, reinicie o serviço Nginx e PHP-FPM para implementar as alterações:

```
systemctl restart php-fpm
 systemctl restart nginx
 ```
**Para garantir vamos Configurar o SELinux e Firewall
 Por padrão, o SELinux é habilitado no CentOS 8. Portanto, você precisará configurar o SELinux para que o Magento funcione corretamente.
 Você pode configurar o SELinux com o seguinte comando:**

```
semanage permissive -a httpd_t
```
**agora vamos criar uma regra de firewall para permitir os serviços de http e https para rede externa**
```
firewall-cmd --permanent --add-service=http
firewall-cmd --permanent --add-service=https
 firewall-cmd --reload
 ```
 <h2> instalação do Wordpress </h2>

**Precisamos criar um novo Banco de dados no MARIADB para o wordpress 
faça login no shell MariaDB com o seguinte comando:**

```
sudo mysql -u root -p
```
**Crie um novo wordpress de banco de dados MariaDB com os comandos SQL abaixo:**
```
CREATE DATABASE bdwp
GRANT ALL ON wordpress.* TO 'wordpress'@'localhost' IDENTIFIED BY 'password';
FLUSH PRIVILEGES;
exit 
```
**Fazendo o Download e instalando o wordpress.**
Navegue ate o diretório cd /var/www/html/ .
baixe o arquivo WordPress mais recente do site oficial do WordPress com o seguinte comando:

```
sudo wget https://wordpress.org/latest.tar.gz
```
altere o proprietário e o grupo do diretório wordpress / e seu conteúdo para nginx da seguinte forma:
```
chown -R nginx:nginx /var/www/html/wordpress
chmod -R 755 /var/www/html/wordpress
```
**Verifique se o Selinux está habilitado , execute o seguinte comando para definir o contexto SElinux para o diretorio /var/www/html/wordpress**

```
sudo semanage fcontext -a -t httpd_sys_rw_content_t \
"/var/www/html/wordpress(/.*)?"
sudo restorecon -Rv /var/www/html/wordpress
```
**Agora, crie um novo arquivo de configuração do nginx wordpress.conf para o WordPress com o seguinte comando:**
```
sudo vim /etc/nginx/conf.d/wordpress.conf
server {
    listen 80;
    server_name blog.robertoseg.tech;

    root /var/www/html/wordpress;
    index index.php;

    access_log /var/log/nginx/wordpress-access.log;
    error_log /var/log/nginx/wordpress-error.log;
    
     location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/run/php-fpm/www.sock;
        fastcgi_index   index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
        expires max;
        log_not_found off;
    }

}

```
**salvar e sair do arquivo,reinicie o serviço nginx**
```
systemctl restart nginx 
```

**Agora utilize o seu dns no navegador para ver a instalação do Wordpress padrão**

<h2>Instalação do TOMCAT proxy reverso<h2>

**Após ter realizado os passos na implantação do servidor-web, vamos instalar o Tomcat**

<h1>Install Java<h1>

```
yum install -y java-11-openjdk-devel
java -version
```
**Criando um usuário para o Tomcat.**
```
useradd -r tomcat
```
**Em seguida, criaremos uma pasta e, em seguida, usaremos o comando “ cd ” para mudar os diretórios para a pasta em que faremos o download do Tomcat.**

```
mkdir /usr/local/tomcat9
cd /usr/local/tomcat9

```
**Agora, vamos fazer o download do arquivo Tomcat usando wget**

```
wget https://archive.apache.org/dist/tomcat/tomcat-9/v9.0.8/bin/apache-tomcat-9.0.8.tar.gz
tar -xvf tomcat9.tar.gz
chown -R tomcat.tomcat /usr/local/tomcat9

```
**Vamos Definir variável de ambiente
Agora, podemos configurar a variável de ambiente CATALINA_HOME usando os seguintes comandos:**

```
echo "export CATALINA_HOME="/usr/local/tomcat9"" >> ~/.bashrc
source ~/.bashrc
```
**Essa variável é configurada para garantir que o acesso ao software seja permitido para todos os usuários do seu sistema.**

```
vim /etc/systemd/system/tomcat.service
```
**Vamos adicionar as seguintes informações ao nosso novo arquivo de unidade.**
```
[Unit]
Description=Apache Tomcat Server
After=syslog.target network.target

[Service]
Type=forking
User=tomcat
Group=tomcat

Environment=CATALINA_PID=/usr/local/tomcat9/temp/tomcat.pid
Environment=CATALINA_HOME=/usr/local/tomcat9
Environment=CATALINA_BASE=/usr/local/tomcat9

ExecStart=/usr/local/tomcat9/bin/catalina.sh start
ExecStop=/usr/local/tomcat9/bin/catalina.sh stop

RestartSec=10
Restart=always
[Install]
WantedBy=multi-user.target
```
**Precisamos salvar o arquivo (usando: wq) e recarregar o serviço para aplicar as alterações.**
```
systemctl daemon-reload
```
**Vamos iniciar o serviço Tomcat e habilitá-lo.**
```
systemctl start tomcat.service && systemctl enable tomcat.service
firewall-cmd --zone=public --permanent --add-port=8080/tcp
```
**Vamos Partir para o teste HAHAHAHA
Abra seu navegador e coloque o ip de sua máquina, setando a porta :8080
EX: http://177.135.78.45:8080**

<h1>Configurar o Nginx como um proxy reverso<h1>


```
 vim /etc/nginx/conf.d/tomcat.conf
```


**Adicione as seguintes linhas:**


```
upstream tomcat {
 server 127.0.0.1:8080 weight=100 max_fails=5 fail_timeout=5;
 }
 
 server {
 listen 80;
 server_name tom.robertoseg.tech;
 
 location / {
 proxy_set_header X-Forwarded-Host $host;
 proxy_set_header X-Forwarded-Server $host;
 proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
 proxy_pass http://tomcat/;
 }
 }
```


**Show..! Agora você pode acessar o servidor Tomcat usando o URL http://seudominio.com sem especificar a porta 8080 do Tomcat.**



<h1>Configurando acesso https<h1>


```
wget https://dl.eff.org/certbot-auto
 mv certbot-auto /usr/local/bin/certbot-auto
 chown root /usr/local/bin/certbot-auto
 chmod 0755 /usr/local/bin/certbot-auto


certbot --nginx -d tom.seudominio.com

/usr/local/bin/certbot-auto --nginx -d seudomio.com
```
**Selecione a opção 2 para redirecionar os trafegos http para https**

<h1>Site Simples<h1>

**Vamos criar uma pasta, com os arquivos do site**

```
mkdir /var/www/html/site

```
**Crie o  arquivo de configuração do virtual host**

```
vim /etc/nginx/conf.d/site.conf
```
Cole as configurações e mude o server_name para o seu dominio.

```
server {
    server_name robertoseg.tech;
    root /var/www/html/site;
    index index.php;

    access_log /var/log/nginx/site-access.log;
    error_log /var/log/nginx/site-error.log;

    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_pass unix:/run/php-fpm/www.sock;
        fastcgi_index   index.php;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico|svg)$ {
        expires max;
        log_not_found off;
    }
}

``` 

**Agora Coloque os sites para https**

```
/usr/local/bin/certbot-auto --nginx -d seudominio.com

```
**Teste todos os sites caso tenha algum que não esteja em https rode o comando novamente apontando para o site desejado. :)**
